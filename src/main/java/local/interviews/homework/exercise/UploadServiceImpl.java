package local.interviews.homework.exercise;

import local.interviews.homework.exercise.detectors.FormatDetector;
import local.interviews.homework.exercise.detectors.dto.FormatDetectorResult;
import local.interviews.homework.exercise.storage.DataStorage;
import local.interviews.homework.exercise.validators.DataValidator;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Stream;

/**
 * 1) применить принципы SOLID для корректировки реализации сервиса потоковой загрузки данных
 * <p>
 * 2) реализовать поддержку JPEG, в будущем поддерживаемых форматов данных будет больше
 * (если не поддерживается формат выбросить IllegalArgumentException)
 * <p>
 * 3) дополнительно реализовать валидацию на максимальную длину потока 1мб (выбросить IllegalStateException)
 * <p>
 * 4) посылать на каждый килобайт загруженных данных в выходной стрим число уже загруженных байт
 * кратно 1024, последним элементом выдавать итоговое число байт
 * примеры:
 * при загрузке 2400 байт в выходном потоке будет 1024, 2048, 2400
 * при загрузке 400 байт в выходном потоке будет 400
 * <p>
 * 5) избегать операций которые полностью буферизируют или входящий или исходящий поток
 * <p>
 * 6) Если пустой поток выбросить IllegalArgumentException
 * <p>
 * PS
 * данная задача не затрагивает моменты связанные с многопоточностью и не требует подобного анализа
 * не реализует реальный пример (как это может быть сделано в реальном решении)
 * воспринимать как просто задание для теста
 */
@Service
@RequiredArgsConstructor
public class UploadServiceImpl implements UploadService {

    private final DataStorage dataStorage;
    private final List<DataValidator> dataValidators;
    private final List<FormatDetector> formatDetectors;

    @Override
    public Stream<Integer> upload(Stream<Byte> data) {
        Stream<Byte> validatedStream = getValidateStream(data);

        FormatDetectorResult formatResult = getFormatDetectorResult(validatedStream);

        return dataStorage.save(formatResult.getStream());
    }


    private Stream<Byte> getValidateStream(Stream<Byte> data) {
        Stream<Byte> dataValidateStream = data;
        for (DataValidator dataValidator : dataValidators) {
            dataValidateStream = dataValidator.validate(dataValidateStream);
        }

        return dataValidateStream;
    }


    private FormatDetectorResult getFormatDetectorResult(Stream<Byte> data) {

        FormatDetectorResult result = new FormatDetectorResult(data, null);

        for (FormatDetector formatValidator : formatDetectors) {
            result = formatValidator.getFormatResult(result.getStream());
            if (result.getName() != null) {
                break;
            }
        }

        if (result.getName() == null) {
            throw new IllegalArgumentException("Сould not determine the loading data format");
        }

        return result;
    }

}




