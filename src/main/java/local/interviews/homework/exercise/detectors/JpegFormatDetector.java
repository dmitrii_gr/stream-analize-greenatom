package local.interviews.homework.exercise.detectors;

import local.interviews.homework.exercise.detectors.dto.FormatDetectorResult;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Spliterators;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import static local.interviews.homework.exercise.enums.Formats.JPEG;

/**
 * Сервис определяет, передаются ли данные в формате JPEG
 */
@Service
@Order(2)
public class JpegFormatDetector implements FormatDetector {

    @Override
    public FormatDetectorResult getFormatResult(Stream<Byte> data) {

        List<Byte> firstBytesFormats = new ArrayList<>();
        Iterator<Byte> iterator = data.iterator();

        while (iterator.hasNext() && firstBytesFormats.size() < JPEG.getMarkerByteCount()) {
            firstBytesFormats.add(iterator.next());
        }

        Stream<Byte> processedStream = firstBytesFormats.stream();
        Stream<Byte> unprocessedStream = StreamSupport.stream(Spliterators.spliteratorUnknownSize(iterator, 0), false);
        Stream<Byte> fullStream = Stream.concat(processedStream, unprocessedStream);

        if (firstBytesFormats.size() == JPEG.getMarkerByteCount()
                && JPEG.getMarkerBytes().equals(firstBytesFormats)) {

            return new FormatDetectorResult(fullStream, JPEG.getFormatName());
        }

        return new FormatDetectorResult(fullStream, null);
    }
}
