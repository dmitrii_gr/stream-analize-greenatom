package local.interviews.homework.exercise.detectors;

import local.interviews.homework.exercise.detectors.dto.FormatDetectorResult;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Spliterators;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import static local.interviews.homework.exercise.enums.Formats.MPEG;

/**
 * Сервис определяет, передаются ли данные в формате MPEG
 */
@Service
@Order(1)
public class MpegFormatDetector implements FormatDetector {

    @Override
    public FormatDetectorResult getFormatResult(Stream<Byte> data) {

        List<Byte> firstBytesFormats = new ArrayList<>();
        Iterator<Byte> iterator = data.iterator();

        while (iterator.hasNext() && firstBytesFormats.size() < MPEG.getMarkerByteCount()) {
            firstBytesFormats.add(iterator.next());
        }

        Stream<Byte> processedStream = firstBytesFormats.stream();
        Stream<Byte> unprocessedStream = StreamSupport.stream(Spliterators.spliteratorUnknownSize(iterator, 0), false);
        Stream<Byte> fullStream = Stream.concat(processedStream, unprocessedStream);

        if (firstBytesFormats.size() == MPEG.getMarkerByteCount()
                && MPEG.getMarkerBytes().equals(firstBytesFormats)) {

            return new FormatDetectorResult(fullStream, MPEG.getFormatName());
        }

        return new FormatDetectorResult(fullStream, null);
    }
}
