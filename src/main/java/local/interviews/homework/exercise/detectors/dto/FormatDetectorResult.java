package local.interviews.homework.exercise.detectors.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.stream.Stream;

/**
 * Возвращаемый результат для детекторов потока
 */
@Getter
@AllArgsConstructor
public class FormatDetectorResult {
    /**
     * Проверяемый стрим
     */
    private Stream<Byte> stream;
    /**
     * Тип данных, определенный детектором
     */
    private String name;
}
