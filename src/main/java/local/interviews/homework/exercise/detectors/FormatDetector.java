package local.interviews.homework.exercise.detectors;

import local.interviews.homework.exercise.detectors.dto.FormatDetectorResult;

import java.util.stream.Stream;

/**
 * Определение формата данных входящего потока
 */
public interface FormatDetector {
    FormatDetectorResult getFormatResult(Stream<Byte> data);
}
