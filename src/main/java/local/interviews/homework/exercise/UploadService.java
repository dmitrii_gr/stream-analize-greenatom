package local.interviews.homework.exercise;

import java.util.stream.Stream;


public interface UploadService {
    Stream<Integer> upload(Stream<Byte> data);
}
