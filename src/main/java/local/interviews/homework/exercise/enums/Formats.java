package local.interviews.homework.exercise.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

/**
 * Список форматов данных
 */
@AllArgsConstructor
@Getter
public enum Formats {
    JPEG("jpeg", List.of((byte) 0xFF, (byte) 0xD8), 2),
    MPEG("mpeg", List.of((byte) 0xFF, (byte) 0xFF, (byte) 0xFF), 3);


    /**
     * Название формата
     */
    private final String formatName;
    /**
     * Набор битов-маркеров, по которым происходит определение формата
     */
    private final List<Byte> markerBytes;
    /**
     * Общее количество битов-маркеров
     */
    private final int markerByteCount;

}
