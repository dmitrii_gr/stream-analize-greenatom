package local.interviews.homework.exercise.validators;

import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;

import java.util.stream.Stream;

/**
 * Валидация на максимальную длину потока равную 1мб.
 * Если превышает выбросить IllegalStateException
 */
@Service
@Order(2)
public class MaxLenghtOneMbDataValidator implements DataValidator {
    private static final int ONE_MB = 1024 * 1024;

    @Override
    public Stream<Byte> validate(Stream<Byte> data) {

        int[] countBytes = new int[]{0};
        return data.peek(aByte -> {
            if (++countBytes[0] > ONE_MB) {
                throw new IllegalStateException("Maximum stream size exceeded 1Mb");
            }
        });

    }
}
