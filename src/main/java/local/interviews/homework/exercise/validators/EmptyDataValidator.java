package local.interviews.homework.exercise.validators;

import lombok.extern.slf4j.Slf4j;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;

import java.util.Iterator;
import java.util.Spliterators;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

/**
 * Проверка на пустой поток
 * Если пустой, то выбросить IllegalArgumentException
 */
@Service
@Order(1)
@Slf4j
public class EmptyDataValidator implements DataValidator {

    @Override
    public Stream<Byte> validate(Stream<Byte> data) {

        Iterator<Byte> iterator = data.iterator();
        if (iterator.hasNext()) {
            return StreamSupport.stream(Spliterators.spliteratorUnknownSize(iterator, 0), false);
        } else {
            throw new IllegalStateException("Empty stream");
        }

    }
}
