package local.interviews.homework.exercise.validators;

import java.util.stream.Stream;

/**
 * Валидация входящего потока
 */
public interface DataValidator {

    Stream<Byte> validate(Stream<Byte> data);

}
