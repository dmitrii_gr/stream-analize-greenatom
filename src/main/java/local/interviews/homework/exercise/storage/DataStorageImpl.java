package local.interviews.homework.exercise.storage;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Stream;


@Service
public class DataStorageImpl implements DataStorage {
    @Override
    public Stream<Integer> save(Stream<Byte> data) {
        AtomicReference<Integer> index = new AtomicReference<>(0);
        List<Integer> integerList = new ArrayList<>();

        data.forEach(aByte -> {
            index.getAndSet(index.get() + 1);

            if (index.get() % 1024 == 0) {
                integerList.add(index.get());
            }

        });

        if (index.get() % 1024 != 0) {
            integerList.add(index.get());
        }

        return integerList.stream();
    }
}


