package local.interviews.homework.exercise.storage;

import java.util.stream.Stream;

/**
 * Хранилище данных
 */
public interface DataStorage {
    Stream<Integer> save(Stream<Byte> data);
}
